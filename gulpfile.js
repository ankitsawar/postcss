var gulp = require('gulp');
var postcss = require("gulp-postcss");
var autoprefixer = require('autoprefixer');
var cssnano = require('cssnano');
var css_modules = require('postcss-modules');


gulp.task('serve', ['css'], function () {
    gulp.watch("./css/*.css", ['css']);
});



gulp.task('watch', ['css'], function () {
    gulp.watch("./css/*.css", ['css']);
});



gulp.task('css', function () {
    var plugin = [
        autoprefixer(),
        css_modules(),
        cssnano()
    ];

    return gulp.src('./css/*.css')
        .pipe(postcss(plugin))
        .pipe(gulp.dest('./dest'));
});

gulp.task('default', ['serve']);