<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>Page Title</title>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" type="text/css" href="dest/main.css">
    <!-- <script src="main.js"></script> -->
</head>
<body>
    <?php
        $path = 'css/main.css.json';
        $strings = json_decode(file_get_contents($path), true);
    ?>
    <div class="<?= $strings['jumbotron'];?>">
        <p class="<?= $strings['classes'];?> <?= $strings['text-center'];?>">PostCss</p>
    </div>
</body>
</html>